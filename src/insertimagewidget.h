/*
  Copyright (c) 2012-2020 Laurent Montel <montel@kde.org>

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This library is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
  License for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; see the file COPYING.LIB.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.

*/
#ifndef KPIMTEXTEDIT_INSERTIMAGEWIDGET_H
#define KPIMTEXTEDIT_INSERTIMAGEWIDGET_H

#include <QUrl>

#include <QWidget>

namespace KPIMTextEdit {
class InsertImageWidgetPrivate;

class InsertImageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit InsertImageWidget(QWidget *parent);
    ~InsertImageWidget();

    void setImageWidth(int value);
    int imageWidth() const;

    void setImageHeight(int value);
    int imageHeight() const;

    QUrl imageUrl() const;
    void setImageUrl(const QUrl &url);

    bool keepOriginalSize() const;

Q_SIGNALS:
    void enableButtonOk(bool enabled);

private:
    friend class InsertImageWidgetPrivate;
    InsertImageWidgetPrivate *const d;
};
}

#endif // KPIMTEXTEDIT_INSERTIMAGEWIDGET_H
