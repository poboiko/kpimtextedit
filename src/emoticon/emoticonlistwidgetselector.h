/*
  Copyright (c) 2019-2020 Laurent Montel <montel@kde.org>

  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This library is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
  License for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; see the file COPYING.LIB.  If not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/

#ifndef EMOTICONLISTWIDGETSELECTOR_H
#define EMOTICONLISTWIDGETSELECTOR_H

#include <QListWidget>
#include "kpimtextedit_private_export.h"

namespace KPIMTextEdit {
class KPIMTEXTEDIT_TESTS_EXPORT EmoticonTextEditItem : public QListWidgetItem
{
public:
    explicit EmoticonTextEditItem(const QString &emoticonText, QListWidget *parent);
    QString text() const;

private:
    QString mText;
};

class KPIMTEXTEDIT_TESTS_EXPORT EmoticonListWidgetSelector : public QListWidget
{
    Q_OBJECT
public:
    explicit EmoticonListWidgetSelector(QWidget *parent = nullptr);
    ~EmoticonListWidgetSelector();

    void setEmoticons(const QList<uint> &lst);
    void setEmoticons(const QStringList &lst);
Q_SIGNALS:
    void itemSelected(const QString &);

private:
    void slotMouseOverItem(QListWidgetItem *item);
    void slotEmoticonClicked(QListWidgetItem *item);
};
}

#endif // EMOTICONLISTWIDGETSELECTOR_H
